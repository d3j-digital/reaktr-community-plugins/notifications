<?php

namespace D3JDigital\Notifications;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Notifications\Contracts\Repositories\NotificationRepositoryInterface;
use D3JDigital\Notifications\Contracts\Services\NotificationServiceInterface;
use D3JDigital\Notifications\Database\Eloquent\Repositories\NotificationRepository;
use D3JDigital\Notifications\Services\NotificationService;

class Plugin extends ReaktrPlugin
{
    public ?string $pluginName = 'Notifications';
    public ?string $pluginKey = 'notifications';
    public ?string $pluginVendorName = 'D3JDigital';
    public ?string $pluginVendorKey = 'd3j-digital';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'Adds notification functionality';
    public ?array $pluginDependencies = [];

    public array $singletons = [
        NotificationRepositoryInterface::class => NotificationRepository::class,
        NotificationServiceInterface::class => NotificationService::class,
    ];

    public function registerRoutes(): void
    {
        $routingService = app(RoutingService::class);
        $routingService->registerApiRoutes('D3JDigital\Notifications\Request\Controllers\Api', __DIR__ . '/Request/Routes/Api/v1.php', 1);
    }

    public function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }
}
