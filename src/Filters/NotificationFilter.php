<?php

namespace D3JDigital\Notifications\Filters;

class NotificationFilter
{
    const PREPARE_NOTIFICATION_RESOURCE = 'plugin.notifications.filter.prepare-notification-resource';
}
