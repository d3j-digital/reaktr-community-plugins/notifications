<?php

namespace D3JDigital\Notifications\Response\Resources;

use OpenSourceDeveloper\Reaktr\Core\Response\Resources\JsonResource;
use D3JDigital\Notifications\Filters\NotificationFilter;
use D3JDigital\Notifications\Response\Entities\NotificationEntity;

/**
 * @mixin NotificationEntity
 */
class NotificationResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => 'notification',
            'attributes' => [
                'id' => $this->id,
                'linked_resource_id' => $this->linkedResourceId,
                'linked_resource_type' => $this->linkedResourceType,
                'user_id' => $this->userId,
                'title' => $this->title,
                'content' => $this->content,
                'read' => $this->read,
                'read_at' => $this->readAt,
                'archived' => $this->archived,
                'archived_at' => $this->archivedAt,
                'created_at' => $this->createdAt,
                'updated_at' => $this->updatedAt,
                'deleted_at' => $this->deletedAt,
            ],
        ];

        return app()->triggerFilter(NotificationFilter::PREPARE_NOTIFICATION_RESOURCE, $data, $this);
    }
}
