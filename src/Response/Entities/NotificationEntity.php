<?php

namespace D3JDigital\Notifications\Response\Entities;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;

class NotificationEntity extends Entity
{
    public ?int $id = null;
    public ?int $linkedResourceId = null;
    public ?string $linkedResourceType = null;
    public ?int $userId = null;
    public ?string $title = null;
    public ?string $content = null;
    public ?bool $read = null;
    public ?string $readAt = null;
    public ?bool $archived = null;
    public ?string $archivedAt = null;
}
