<?php

namespace D3JDigital\Notifications\Contracts\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Contracts\Repositories\Repository;

interface NotificationRepositoryInterface extends Repository
{

}
