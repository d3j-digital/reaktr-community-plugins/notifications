<?php

namespace D3JDigital\Notifications\Contracts\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;

interface NotificationServiceInterface
{
    /**
     * @param array $relations
     * @param array|string[] $columns
     * @param bool $paginate
     * @return LengthAwarePaginator|Collection
     */
    public function getNotifications(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection;

    /**
     * @param int $id
     * @param array $relations
     * @param array|string[] $columns
     * @return Entity|null
     */
    public function getNotification(int $id, array $relations = [], array $columns = ['*']): ?Entity;

    /**
     * @param array $attributes
     * @return Entity|null
     */
    public function createNotification(array $attributes): ?Entity;

    /**
     * @param int $id
     * @param array $attributes
     * @param bool $withoutLoadingModel
     * @return Entity|null
     */
    public function updateNotification(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity;

    /**
     * @param int|array $ids
     * @param bool $forceDelete
     */
    public function deleteNotification(int|array $ids, bool $forceDelete = false): void;
}
