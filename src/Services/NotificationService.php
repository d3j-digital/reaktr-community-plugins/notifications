<?php

namespace D3JDigital\Notifications\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;
use OpenSourceDeveloper\Reaktr\Core\Response\PaginationResponse;
use D3JDigital\Notifications\Contracts\Services\NotificationServiceInterface;
use D3JDigital\Notifications\Database\Eloquent\Repositories\NotificationRepository;
use D3JDigital\Notifications\Response\Entities\NotificationEntity;

class NotificationService implements NotificationServiceInterface
{
    public function __construct(
        protected NotificationRepository $notificationRepository,
    ) {}

    public function getNotifications(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection
    {
        $result = $this->notificationRepository->all($relations, $columns, $paginate);
        if ($result instanceof PaginationResponse) {
            return new LengthAwarePaginator(collect($result->getRecords())->mapInto(NotificationEntity::class), $result->getRecordCount(), $result->getRecordsPerPage(), $result->getCurrentPage());
        }
        return collect($result)->mapInto(NotificationEntity::class);
    }

    public function getNotification(int $id, array $relations = [], array $columns = ['*']): ?Entity
    {
        $result = $this->notificationRepository->find($id, $relations, $columns);
        return $result ? (new NotificationEntity)->fill($result) : null;
    }

    public function createNotification(array $attributes): ?Entity
    {
        $result = $this->notificationRepository->create($attributes);
        return $result ? (new NotificationEntity)->fill($result) : null;
    }

    public function updateNotification(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity
    {
        $result = $this->notificationRepository->update($id, $attributes, $withoutLoadingModel);
        return $result ? (new NotificationEntity)->fill($result) : null;
    }

    public function deleteNotification(int|array $ids, bool $forceDelete = false): void
    {
        $this->notificationRepository->delete($ids, $forceDelete);
    }
}
