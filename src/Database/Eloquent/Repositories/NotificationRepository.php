<?php

namespace D3JDigital\Notifications\Database\Eloquent\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Repository;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\SystemModel;
use D3JDigital\Notifications\Contracts\Repositories\NotificationRepositoryInterface;
use D3JDigital\Notifications\Database\Eloquent\Models\Notification;

class NotificationRepository extends Repository implements NotificationRepositoryInterface
{
    /**
     * @return Model|SystemModel
     */
    function model(): Model|SystemModel
    {
        return new Notification();
    }
}
