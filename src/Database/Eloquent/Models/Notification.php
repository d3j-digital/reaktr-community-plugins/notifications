<?php

namespace D3JDigital\Notifications\Database\Eloquent\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;

class Notification extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'notifications';

    /**
     * @var array
     */
    protected $guarded = [];
}
