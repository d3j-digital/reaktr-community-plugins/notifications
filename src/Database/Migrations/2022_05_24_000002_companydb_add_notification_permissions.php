<?php

use Illuminate\Database\Migrations\Migration;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\AuthService;
use OpenSourceDeveloper\Reaktr\Core\Base\PermissionObject;

class CompanydbAddNotificationPermissions extends Migration
{
    protected AuthService $authService;

    public function __construct()
    {
        $this->authService = app(AuthService::class);
    }

    public function up(): void
    {
        $permissions = [
            new PermissionObject('notifications.*', 'reaktr.permissions.global.all_actions', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
            new PermissionObject('notifications.index', 'reaktr.permissions.global.list', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
            new PermissionObject('notifications.store', 'reaktr.permissions.global.create', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
            new PermissionObject('notifications.show', 'reaktr.permissions.global.view', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
            new PermissionObject('notifications.update', 'reaktr.permissions.global.update', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
            new PermissionObject('notifications.destroy', 'reaktr.permissions.global.delete', ['resource' => 'plugin-notifications.resources.notification.name.singular']),
        ];

        foreach ($permissions as $permission) {
            $this->authService->createPermission($permission);
        }
    }
}
