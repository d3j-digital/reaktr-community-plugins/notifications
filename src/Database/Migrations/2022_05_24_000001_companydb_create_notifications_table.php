<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CompanydbCreateNotificationsTable extends Migration
{
    public function up(): void
    {
        if (!Schema::hasTable('notifications')) {
            Schema::create('notifications', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('linked_resource_id')->nullable();
                $table->string('linked_resource_type')->nullable();
                $table->unsignedBigInteger('user_id');
                $table->string('title');
                $table->longText('content')->nullable();
                $table->json('meta')->nullable();
                $table->boolean('read')->default(false);
                $table->timestamp('read_at')->nullable();
                $table->boolean('archived')->default(false);
                $table->timestamp('archived_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_id')->references('id')->on('core_users')->onDelete('cascade');
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
}
