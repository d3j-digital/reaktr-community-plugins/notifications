<?php

namespace D3JDigital\Notifications\Request\Validation;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNotification extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'sometimes|int',
            'title' => 'sometimes|string',
            'content' => 'sometimes|string',
            'read' => 'sometimes|boolean',
            'archived' => 'sometimes|boolean',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'this field is required',
        ];
    }
}
