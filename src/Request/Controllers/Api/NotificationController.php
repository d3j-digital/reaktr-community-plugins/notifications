<?php

namespace D3JDigital\Notifications\Request\Controllers\Api;

use D3JDigital\Notifications\Response\Resources\NotificationResource;
use Illuminate\Http\JsonResponse;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Controller;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Notifications\Contracts\Services\NotificationServiceInterface;
use D3JDigital\Notifications\Request\Validation\StoreNotification;
use D3JDigital\Notifications\Request\Validation\UpdateNotification;

class NotificationController extends Controller
{
    /**
     * @param RoutingService $routingService
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(
        protected RoutingService $routingService,
        protected NotificationServiceInterface $notificationService,
    ) {
        parent::__construct();
        $this->middleware('UserIsAuthenticated:jwt');
        $this->middleware('UserIsAuthorised:notifications.index')->only('index');
        $this->middleware('UserIsAuthorised:notifications.show')->only('show');
        $this->middleware('UserIsAuthorised:notifications.store')->only('store');
        $this->middleware('UserIsAuthorised:notifications.update')->only('update');
        $this->middleware('UserIsAuthorised:notifications.destroy')->only('destroy');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(200, '', NotificationResource::collection($this->notificationService->getNotifications($this->relations, $this->columns)));
    }

    /**
     * @param StoreNotification $request
     * @return JsonResponse
     */
    public function store(StoreNotification $request): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(201, '', new NotificationResource($this->notificationService->createNotification($request->validated())));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        if ($resource = $this->notificationService->getNotification($id, $this->relations, $this->columns)) {
            return $this->routingService->renderJsonSuccessResponse(200, '', new NotificationResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(404);
    }

    /**
     * @param UpdateNotification $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateNotification $request, int $id): JsonResponse
    {
        if ($resource = $this->notificationService->updateNotification($id, $request->validated())) {
            return $this->routingService->renderJsonSuccessResponse(202, '', new NotificationResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(204);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->notificationService->deleteNotification($id);
        return $this->routingService->renderJsonSuccessResponse(204);
    }
}
